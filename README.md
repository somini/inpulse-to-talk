# InPulse to Talk

Push-to-Talk with libinput + Pulseaudio.

This is not *strictly* push-to-talk, but it works as so in practice. The
default status is muting all inputs, and un-muting them only when the key is
pressed (after the `KeyPress` event, and before the `KeyRelease` event).

This uses `libinput` to monitor the keyboard and a variety of audio control
interfaces to control the audio part.

## Installation

This is available on AUR: https://aur.archlinux.org/packages/inpulse-to-talk/

It should work out of the box using the `polkit` implementation. Make sure your
user is on the `wheel` group, this is technically a keylogger. Permission to
run `inpulse-daemon` without password is not to be taken lightly.

To start this on login you can use the `contrib/inpulse-to-talk.desktop` file
as an autostart configuration.
Copy it to `~/.config/autostart` and it should work on most desktops. Check the
`Exec` line if you need to pass special arguments to the file.
You can use the same file as a regular desktop file. Copy that file to
`~/.local/share/applications/`.

## Usage

You need a keycode to be used as Push-To-Talk key. The best way to check this is using:

```sh
$ xev -event keyboard
```

Press the key you want, and look for the `KeyPress` event, check the `keycode` parameter.

Then call the script like this (depends on audio API):

```sh
$ inpulse-wrapper $KEYCODE --pressed=true --released=false -- pulseaudio-ctl mute-input
$ inpulse-wrapper $KEYCODE --pressed=1 --released=0 -- wpctl set-mute "@DEFAULT_AUDIO_SOURCE@"
```

This requires `inpulse-daemon` to be in your `$PATH`, and that runs as `sudo`.

There's a wrapper for this particular usage supporting both audio API commands,
`inpulse-to-talk`.

### Privilege Escalation

Since `inpulse-daemon` is technically a keylogger, it needs special privileges
to run. Since this is a background service, it should run without prompt. There
are 3 alternatives:

1. Configure polkit, see the `polkit` folder. The AUR package implements this
   out of the box, see specially the patch to be applied. 

2. Use `sudo`. This is the default for development (`make test-events-run`).
   Configure `sudoers` to allow `sudo` to escalate privileges without password.

3. Add your user to the `input` group, with `sudo gpasswd -a $USER input`. Note
   this allows your user to eavesdrop unprompted on all input events.

### Notifications

If you want to be notified when the script changes the mute state, run
`inpulse-to-talk-notifications` instead of `inpulse-to-talk`.

This is spammy, since it doesn't replace the old notification, just creates new
ones. Improvements to the `inpulse-to-talk-notify` script are welcome, but left
as an exercise to the reader.
