# Include PATH:
# - Current Folder
# - Rust target folder
export PATH := $(PWD)/target/debug:$(PWD):$(PATH)

.PHONY: build
build:
	cargo build

.PHONY: test-run test-notifications test-echo test-help
# <Super>: 125
test-run: build
	inpulse-to-talk 125
test-notifications: build
	inpulse-to-talk-notifications 125
test-echo: build
	inpulse-wrapper 125 -- echo

test-help: build
	inpulse-daemon -h
