use std::io::{stdout, Write};
use std::fs::{File, OpenOptions};
use std::os::fd::AsRawFd;
use std::os::unix::{fs::OpenOptionsExt, io::OwnedFd};
use std::path::Path;

use libc::{signal, SIGPIPE, SIG_DFL};
use input::{Libinput, LibinputInterface};
use input::event::Event::{Keyboard};
use input::event::keyboard::KeyboardEvent::{Key};
use input::event::keyboard::KeyState;
use input::event::keyboard::KeyboardEventTrait;
use nix::poll::{poll, PollFlags, PollFd};

struct Interface;

#[macro_use]
extern crate clap;

impl LibinputInterface for Interface { //{{{
    fn open_restricted(&mut self, path: &Path, flags: i32) -> Result<OwnedFd, i32> {
        OpenOptions::new()
            .custom_flags(flags)
            // Open as Read-Only, always
            .read(true).write(false)
            .open(path)
            .map(|file| file.into())
            .map_err(|err| err.raw_os_error().unwrap())
    }
    fn close_restricted(&mut self, fd: OwnedFd) {
        drop(File::from(fd))
    }
} //}}}

fn main() {
    let matches = clap::App::new("inpulse-daemon")
        .author(crate_authors!("\n"))
        .version(crate_version!())
        .about("Daemon to collect libinut event")
        .arg(clap::Arg::with_name("keycode")
             .value_name("KEYCODE")
             .takes_value(true)
             .help("The keycode to listen to")
        )
        .arg(clap::Arg::with_name("seat")
             .long("seat")
             .env("XDG_SEAT")
             .default_value("seat0")
             .help("The libinput seat to connect to.")
        )
        .arg(clap::Arg::with_name("pressed")
             .long("pressed")
             .default_value("↓")
             .help("The string to output when keycode is pressed.")
        )
        .arg(clap::Arg::with_name("released")
             .long("released")
             .default_value("↑")
             .help("The string to output when keycode is released.")
        )
        .get_matches();
    let kid: u32 = value_t_or_exit!(matches, "keycode", u32);
    let seat = matches.value_of("seat").unwrap();
    let str_pressed = matches.value_of("pressed").unwrap();
    let str_released = matches.value_of("released").unwrap();

    let mut input = Libinput::new_with_udev(Interface);
    input.udev_assign_seat(seat).unwrap();

    // Die when the process reading the output goes away
    unsafe {
        signal(SIGPIPE, SIG_DFL);
    }

    // Setup the polling file descriptor
    let pollfd = PollFd::new(input.as_raw_fd(), PollFlags::POLLIN);

    eprintln!("Listen to {} @ '{}'", kid, seat);
    let mut stdout = stdout();
    while poll(&mut [pollfd], -1).is_ok() {
        input.dispatch().unwrap();
        for event in &mut input {
            // TODO: Make sure stdout is open and writable?
            if let Keyboard(Key(eventk)) = &event {
                if eventk.key() == kid {
                    let output: &str = match eventk.key_state() {
                        KeyState::Pressed => str_pressed,
                        KeyState::Released => str_released,
                    };
                    println!("{}", output);
                    stdout.flush().unwrap();
                    // Make sure to flush stdout
                    // not sure this is necessary...
                }
            }
        }
    }
}


// vim: fdm=marker:
